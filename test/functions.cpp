#include <catch2/catch.hpp>

#include "../src/fixedpoint.hpp"
#include "../src/functions.hpp"

// Free standing function tests
TEST_CASE("Sqrt approximation works") {
    SECTION("for 25") {
        Fp<4> input{25.};

        auto result = approx_sqrt<4>(input);

        REQUIRE(result.as_float() == 5);
    }
    SECTION("for 36") {
        Fp<4> input{36.};

        auto result = approx_sqrt<4>(input);

        REQUIRE(result.as_float() == 6);
    }
    SECTION("for 1 million") {
        Fp<4> input{1000000.};

        auto result = approx_sqrt<4>(input);

        REQUIRE(result.as_float() == 1000);
    }
    SECTION("One in the LSB of the input and output") {
        Fp<2> input{6.25};

        auto result = approx_sqrt<3>(input);
        Fp<1> expected{2.5};

        INFO(input.as_float());
        INFO(result._raw_value());
        INFO(expected._raw_value());
        REQUIRE(result == expected);
    }

    SECTION("When output bits is not the same as input bits") {
        Fp<4> input{25};

        auto result = approx_sqrt<8>(input);
        Fp<8> expected{5};

        INFO(input.as_float());
        INFO(result._raw_value());
        INFO(expected._raw_value());
        REQUIRE(result == expected);
    }

    SECTION("With fewer output bits than input bits") {
        Fp<8> input{25};

        auto result = approx_sqrt<4>(input);
        Fp<4> expected{5};

        INFO(input.as_float());
        INFO(result._raw_value());
        INFO(expected._raw_value());
        REQUIRE(result == expected);
    }

    SECTION("For 257.9916...") {
        auto input = Fp<25>::from_raw(8656764303);

        auto result = approx_sqrt<30>(input);
        Fp<30> expected{16.062119430621991};

        INFO(input.as_float());
        INFO(result._raw_value());
        INFO(result.as_float());
        INFO(expected._raw_value());
        INFO(expected.as_float());
        REQUIRE(result == expected);
    }
}
