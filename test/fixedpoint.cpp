#include <catch2/catch.hpp>

#include "../src/fixedpoint.hpp"

TEST_CASE("FP can be initialised from floats") {
    auto result = Fp<10>((float) 1.2);

    REQUIRE(result.as_float() == Approx(1.19921875));

}

TEST_CASE("Initialisation from raw value works") {
    auto result = Fp<5>::from_raw(1 << 4);

    REQUIRE(result.as_float() == 0.5);
}

TEST_CASE("FP can be initialised from doubles") {
    auto result = Fp<5>((double) 1.5);
    CHECK(result.as_float() == Approx(1.5));
}

TEST_CASE("Large FPs can be initialised from 64 bit ints") {
    auto result = Fp<5>((int64_t) 4294967297ull);
    CHECK(result.as_float() == Approx(4294967300ull));
}

TEST_CASE("Construction works for different sized Fp classes") {
    auto a = Fp<5>(10);
    auto result = Fp<10>(a);

    REQUIRE(result.as_float() == 10);
}

TEST_CASE("Addition works for same size FPs") {
    auto a = Fp<5>(1.5);
    auto b = Fp<5>(0.5);
    auto result = a + b;

    REQUIRE(a.as_float() == 1.5);
    REQUIRE(b.as_float() == 0.5);
    REQUIRE(result.as_float() == 2.0);
}

TEST_CASE("Addition works for varying size FPs") {
    auto a = Fp<0>(1);
    auto b = Fp<5>(0.5);
    auto result = a + b;

    // Ensure that the operator didn't modify the original values
    REQUIRE(a.as_float() == 1);
    REQUIRE(b.as_float() == 0.5);
    REQUIRE(result.as_float() == 1.5);
}

TEST_CASE("Subtraction works for varying size FPs") {
    auto a = Fp<0>(1);
    auto b = Fp<5>(0.5);
    auto result = a - b;

    // Ensure that the operator didn't modify the original values
    REQUIRE(a.as_float() == 1);
    REQUIRE(b.as_float() == 0.5);
    REQUIRE(result.as_float() == 0.5);
}

TEST_CASE("Multiplication works for varying size FPs") {
    auto a = Fp<0>(2);
    auto b = Fp<5>(0.5);
    auto result = a * b;

    // Ensure that the operator didn't modify the original values
    REQUIRE(a.as_float() == 2);
    REQUIRE(b.as_float() == 0.5);
    REQUIRE(result.as_float() == 1);
}

TEST_CASE("Division works for varying size FPs") {
    auto a = Fp<0>(2);
    auto b = Fp<5>(0.5);
    auto result = a / b;

    // Ensure that the operator didn't modify the original values
    REQUIRE(a.as_float() == 2);
    REQUIRE(b.as_float() == 0.5);
    REQUIRE(result.as_float() == 4);
}

TEST_CASE("Boolean operators work") {
    auto a = Fp<0>(2);
    auto b = Fp<5>(0.5);
    auto c = Fp<5>(0.5);

    REQUIRE((b < a) == true);
    REQUIRE((b < c) == false);
    REQUIRE((a <= c) == false);
    REQUIRE((b <= c) == true);
    REQUIRE((b >= c) == true);
    REQUIRE((a >= c) == true);
    REQUIRE((a > c) == true);
    REQUIRE((b > c) == false);
}

TEST_CASE("Assignment operator works for same size") {
    auto a = Fp<3>(2);
    auto b = Fp<3>(4);

    a = b;

    REQUIRE(a.as_float() == 4);
}
TEST_CASE("Assignment operator works for different sizes") {
    auto a = Fp<3>(2);
    auto b = Fp<2>(4);

    a = b;

    REQUIRE(a.as_float() == 4);
}


TEST_CASE("Casting to lower bit count type works") {
    auto a = Fp<10>(2);
    auto result = a.cast<5>();

    REQUIRE(result.as_float() == 2);
}

TEST_CASE("Integer conversion works") {
    auto a = Fp<10>(2.3);
    auto b = Fp<10>(2.7);
    auto c = Fp<10>(2);

    REQUIRE(a.as_int() == 2);
    REQUIRE(b.as_int() == 3);
    REQUIRE(c.as_int() == 2);
}

TEST_CASE("Sqrt works") {
    auto a = Fp<10>(4);

    auto result = a.sqrt();

    REQUIRE(result.as_float() == 2);
}

TEST_CASE("Unary sub works") {
    auto a = Fp<5>(16);

    REQUIRE((-a).as_float() == -16);
}



// Bug tests
TEST_CASE("10/0.33 produces correct value") {
    auto a = Fp<24>(10);
    auto b = Fp<24>(0.33);

    double max_error = 10 * pow(2, -24) / (0.33 + pow(2, -24));

    REQUIRE(std::abs((a/b).as_float() - (10/0.33)) < max_error);
}

TEST_CASE("245.6 * 375 produces correct value") {
    auto a = Fp<24>(245.6);
    auto b = Fp<24>(375);

    double max_error = 245.6 * pow(2, -24) + 375 * pow(2, -24);

    REQUIRE(std::abs((a*b).as_float() - (245.6 * 375)) < max_error);
}

TEST_CASE("92100 / 15.6 produces correct value") {
    auto a = Fp<24>(92100);
    auto b = Fp<24>(15.6);

    double max_error = 91200 * pow(2, -24) / (15.6 + pow(2, -24));

    REQUIRE(std::abs((a/b).as_float() - (92100/15.6)) < max_error);
}

