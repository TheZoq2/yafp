import gdb

class FpPrinter(object):
    """Print a WORD_DESC"""
    def __init__(self, val):
        self.val = val

    def to_string(self):
        template_args = int(str(self.val.type).split('<')[1][:-1])

        raw_val=int(self.val['value'])
        float_val = raw_val / 2**template_args

        has_debug = any(map(lambda f: f.name == 'debug_value', self.val.type.fields()))
        if has_debug:
            debug = f", debug_value = {self.val['debug_value']}"
        else:
            debug = ""
        return f"{self.val.type}: (val = {float_val}, raw = {raw_val}{debug})"

def pretty_fp (val):
    if str(val.type).split("<")[0] == 'Fp' or str(val.type).split("<")[0] == 'const Fp':
        return FpPrinter(val)
    return None

gdb.pretty_printers.append(pretty_fp)
