CC := clang++

CXXFLAGS=-g -DYAFP_DEBUG

HEADERS=${wildcard src/*.hpp}
OBJS=${patsubst test/%.cpp, build/test/%.o, ${wildcard test/*.cpp}}
MAIN_OBJ=build/test/main.o

run: build/test/runner .PHONY
	@echo -e "[\033[0;34mrunner\033[0m] Running test"
	@$(DEBUGGER) ./build/test/runner


build/test/runner: ${OBJS} ${MAIN_OBJ}
	@echo -e "[\033[0;34m$(CC)\033[0m] Linking"
	@mkdir -p ${@D}
	@${CC} ${CXXFLAGS} ${MAIN_OBJ} ${OBJS} -o $@

build/test/%.o: test/%.cpp ${HEADERS} Makefile
	@echo -e "[\033[0;34m$(CC)\033[0m] Building $@"
	@mkdir -p ${@D}
	@${CC} ${CXXFLAGS} $< -c -o $@

${MAIN_OBJ}: test/main/main.cpp Makefile
	@echo -e "[\033[0;34m$(CC)\033[0m] Building main"
	@mkdir -p ${@D}
	@${CC} ${CXXFLAGS} test/main/main.cpp -c -o $@


clean:
	rm -rf build

.PHONY:
