#pragma once


#include <cmath>

#include <iostream>
#include <type_traits>

typedef int64_t internal_type ;

// Macros used for detecting errors inside the FP class if YAFP_DEBUG is set.
// If an error is detected, execution will be aborted
#ifdef YAFP_DEBUG
    #define DEBUG_CHECK_RESULT \
        bool in_threshold = \
            std::abs(this->debug_value - this->as_float()) > RESULT_THRESHOLD; \
        if(in_threshold) { \
            printf("YAFP value missmatch: expected: %f, got: %f\n", \
                this->debug_value, \
                this->as_float() \
            ); \
            throw "YAFP debug missmatch"; \
        }
    #define DO_DEBUG(...) \
        float old_debug_value = debug_value; \
        IF_DEBUG(__VA_ARGS__); \
        DEBUG_CHECK_RESULT
    #define IF_DEBUG(...) \
        __VA_ARGS__
#else
    #define DO_DEBUG(...)
    #define IF_DEBUG(...)
#endif

// Implements a constructor which creates an Fp from a nother datatype
#define impl_constructor(datatype) \
    Fp(const datatype& value) \
        : value(value * pow(2, FracBits)) \
        IF_DEBUG(,debug_value(value)) \
        {}

float const RESULT_THRESHOLD = pow(2, -6);


template <int FracBits> 
class Fp {
    public:
        // Creates a new instance using the raw data provided
        static Fp<FracBits> from_raw(int64_t raw) {
            auto result = Fp();
            result.value = raw;
            IF_DEBUG(result.debug_value = result.as_float());
            return result;
        }

        // Allow uninitialised values, but not implcitly
        explicit Fp() {}
        // Allow copy from the same type
        Fp(const Fp<FracBits>& other) {
            this->value = other.value;
            DO_DEBUG(this->debug_value = other.debug_value)
        }
        // Allow implicit copy if no precision is lost
        template
            < int OtherBits
            , typename = typename std::enable_if_t<(OtherBits <= FracBits)>
            >
        Fp(const Fp<OtherBits>& other) {
            this->value = other._raw_value() << (FracBits - OtherBits);
            IF_DEBUG(this->debug_value = other.debug_value);
        }
        impl_constructor(int8_t);
        impl_constructor(int16_t);
        impl_constructor(int32_t);
        impl_constructor(int64_t);
        impl_constructor(float);
        impl_constructor(double);

        // Destructive cast to lower fractional bit counts
        template
            < int OtherBits
            , typename = typename std::enable_if_t<(OtherBits <= FracBits)>
            >
        Fp<OtherBits> cast() const {
            auto result = Fp<OtherBits>();
            result._set_raw_value(this->value >> (FracBits - OtherBits));
            DO_DEBUG(result.debug_value = this->debug_value);
            return result;
        }
        // Casts to higher fractional bit counts is lossless and can
        // be done using the constructor
        template
            < int OtherBits
            , typename = typename std::enable_if_t<(OtherBits > FracBits)>
            >
        Fp<OtherBits> cast(...) const {
            return (Fp<OtherBits>(*this));
        }

        bool operator==(const Fp<FracBits>& other) const {
            return other.value == this->value;
        }
        bool operator!=(const Fp<FracBits>& other) const {
            return !(*this == other);
        }


        // Assignment to same type
        Fp<FracBits> operator=(const Fp<FracBits>& other) {
            this->value = other.value;
            DO_DEBUG(this->debug_value = other.debug_value);
            return *this;
        }
        // Assignment to other fractional bit counts is only allowed
        // if the result has more precision. Otherwise, use `cast()`
        template
            < int OtherBits
            , typename = typename std::enable_if_t<(OtherBits < FracBits)>
            >
        Fp<FracBits>& operator=(const Fp<OtherBits>& other) {
            this->value = other._raw_value() << (FracBits - OtherBits);
            DO_DEBUG(this->debug_value = other.debug_value);
            return *this;
        }

        float as_float() const {
            return this->value / pow(2, FracBits);
        }

        double as_double() const {
            return this->value / pow(2, FracBits);
        }

        internal_type as_int() const {
            // Check if the value is odd or even
            auto shifted = this->value >> (FracBits - 1);
            auto is_even = ((int8_t) shifted & 0x01) == 1;
            if(is_even) {
                return (this->value >> FracBits) + 1;
            }
            return this->value >> FracBits;
        }

        Fp<0> truncate() {
            auto result = Fp<0>();
            result._set_raw_value(this->value >> FracBits);
            DO_DEBUG(result.debug_value = this->value >> FracBits);
            return result;
        }

        // Computes the square root of the value. Internally casts to float
        // and back
        Fp<FracBits> sqrt() const {
            return Fp<FracBits>(::sqrt(as_float()));
        }

        Fp<FracBits> abs() const {
            return Fp::from_raw(std::abs(this->value));
        }


        // Helper functions for the IMPL_BINOP macro
        void add(const Fp<FracBits>& other) {
            this->value += other.value;
            DO_DEBUG(this->debug_value += other.debug_value)
        }
        void sub(const Fp<FracBits>& other) {
            this->value -= other.value;
            DO_DEBUG(this->debug_value -= other.debug_value)
        }
        void mul(const Fp<FracBits>& other) {
            this->value = ((__int128_t)this->value * (__int128_t)other.value) >> FracBits;
            DO_DEBUG(this->debug_value *= other.debug_value)
        }
        void div(const Fp<FracBits>& other) {
            auto this_value = (__int128_t) this->value << (FracBits);
            auto other_value = other.value;
            if(other_value == 0) {
                std::cerr << "[warn] Division by 0. Returning 0" << std::endl;
                this->value = 0;
            }
            else {
                this->value = (this_value / other_value);
            }
            DO_DEBUG(this->debug_value /= other.debug_value)
        }

        #define WRAP_BOOL_OPERATOR(OP, NAME) \
            bool NAME(const Fp<FracBits>& other) const { \
                return this->value OP other.value; \
            }
        WRAP_BOOL_OPERATOR(<, less_than);
        WRAP_BOOL_OPERATOR(<=, less_than_or_eq);
        WRAP_BOOL_OPERATOR(>=, greater_than_or_eq);
        WRAP_BOOL_OPERATOR(>, greater_than);

        // Unary subtraction
        Fp<FracBits> operator-() const {
            auto result = Fp<FracBits>();
            result.value = -this->value;
            DO_DEBUG(result.debug_value = -this->debug_value)
            return result;
        }

        // Accessors required in order to access private value of instances
        // with other amounts of fractional bits
        internal_type _raw_value() const {return value;}
        void _set_raw_value(internal_type value) {
            this->value = value;
            DO_DEBUG(this->debug_value = this->as_float())
        }
    private:
        // Internal value storage
        internal_type value;

        IF_DEBUG(public: double debug_value);
};

#define IMPL_BINOP(operator, internal_function) \
    template<int LF, int RF, int OF = std::max(LF, RF)> \
    Fp<OF> operator(const Fp<LF>& lhs, const Fp<RF>& rhs) {\
        Fp<OF> result = lhs; \
        result.internal_function((Fp<OF>) rhs); \
        return result; \
    }

IMPL_BINOP(operator+, add);
IMPL_BINOP(operator-, sub);
IMPL_BINOP(operator*, mul);
IMPL_BINOP(operator/, div);

#define IMPL_BOOL_BINOP(operator, internal_function) \
    template<int LF, int RF, int OF = std::max(LF, RF)> \
    bool operator(const Fp<LF>& lhs, const Fp<RF>& rhs) {\
        return ((Fp<OF>) lhs).internal_function((Fp<OF>) rhs); \
    }
IMPL_BOOL_BINOP(operator<, less_than)
IMPL_BOOL_BINOP(operator<=, less_than_or_eq)
IMPL_BOOL_BINOP(operator>=, greater_than_or_eq)
IMPL_BOOL_BINOP(operator>, greater_than)
