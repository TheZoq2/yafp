#pragma once

#include "fixedpoint.hpp"

#include <vector>
#include <optional>

// Approximate square the square root using a restoring binary add and subtract
// algorithm. Based off http://medialab.freaknet.org/martin/src/sqrt/sqrt.c
template<int O, int F>
Fp<O> approx_sqrt(Fp<F> input) {
    int required_input_bits = O * 2;
    __int128_t input_ = static_cast<__int128_t>(input._raw_value()) << (required_input_bits - F);
    __int128_t result = 0;
    __int128_t bit = static_cast<__int128_t>(1) << 126ll;

    // This was present in the original algorithm, but is not required for an
    // FPGA implementation as hardware for the full calculation would
    // be required in the worst case
    // while(bit >= input_) {
    //     bit >>= 2;
    // }

    while(bit != 0) {
        __int128_t sub = input_ - (result + bit);
        if(sub >= 0) {
            input_ = sub;
            result = (result >> 1) + bit;
        }
        else {
            result >>= 1;
        }
        bit >>= 2;
    }
    return Fp<O>::from_raw(result);
}
