# YAFP: Yet Another Fixed Point library

YAFP is a c++ fixed point library designed to make safe casts implicit and
unsafe casts explicit.
